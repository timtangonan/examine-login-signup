import Image from "next/image";

const NavbarMobile = () => {
  return (
    <nav className="h-14 flex lg:hidden bg-purple-600">
      <div className="px-3 grid place-items-center">
        <div className="w-8 h-8 grid place-items-center bg-purple-400 rounded">
          <Image
            src="/hamburger-mobile.png"
            alt="menu buton"
            width="20"
            height="14"
          />
        </div>
      </div>
      <div className="grow flex items-center">
        <Image
          src="/examine-logo-mobile.png"
          alt="Examine logo"
          width="72"
          height="16"
        />
      </div>
      <div className="grid place-items-center px-3">
        <Image
          src="/magnifying-glass-m.png"
          alt="magnifying glass"
          width="16"
          height="16"
        />
      </div>
    </nav>
  );
};

export default NavbarMobile;
