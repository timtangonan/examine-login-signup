import { useState } from "react";

import Image from "next/image";
import Link from "next/link";

const LoginCard = () => {
  const [passwordShow, setPasswordShow] = useState(false);

  const togglePasswordShow = () => {
    setPasswordShow(!passwordShow);
  };

  return (
    <div className="grid place-items-center my-24">
      <div className="p-2 lg:shadow-lg lg:rounded-3xl lg:p-20">
        <div className="flex flex-col items-center">
          <h1 className="text-5xl font-serif text-center">
            Welcome back!{" "}
            <span role="img" aria-label="wave">
              👋
            </span>
          </h1>
          <span className="text-gray-600">Let's build something great</span>
          <div className="flex gap-4 justify-between mt-10">
            <div className="grid place-items-center shadow rounded-lg w-16 h-14">
              <a
                href="https://google.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image
                  src="/Google-logo.png"
                  alt="Google logo"
                  width="23"
                  height="23"
                />
              </a>
            </div>
            <div className="grid place-items-center shadow rounded-lg w-16 h-14">
              <a
                href="https://apple.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image
                  src="/Apple-logo.png"
                  alt="Apple logo"
                  width="19.52"
                  height="23.21"
                />
              </a>
            </div>
            <div className="grid place-items-center shadow rounded-lg w-16 h-14">
              <a
                href="https://facebook.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image
                  src="/Facebook-logo.png"
                  alt="Facebook logo"
                  width="14.68"
                  height="34.82"
                />
              </a>
            </div>
          </div>
          <div className="relative mt-8 border-t-2 border-[#EDF2F7]">
            <div className="flex justify-center">
              <span className="absolute block uppercase -mt-3 bg-white px-4 text-gray-600">
                or
              </span>
            </div>
            <form action="" className="min-w-[327px] lg:min-w-[500px]">
              <input
                type="text"
                name=""
                id="email"
                placeholder="Email"
                className="w-full border-2 border-gray-400 mt-6 py-2 px-4 rounded focus:outline-none"
              />
              <div className="w-full flex justify-between border-2 border-gray-400 mt-6 py-2 px-4 rounded">
                <input
                  type={passwordShow ? "text" : "password"}
                  name=""
                  id="password"
                  placeholder="Password"
                  className="w-full focus:outline-none"
                />
                <span
                  className="uppercase font-bold text-primary cursor-pointer"
                  onClick={togglePasswordShow}
                >
                  show
                </span>
              </div>
              <button className="w-full rounded mt-6 py-4 bg-primary text-white">
                Login
              </button>
            </form>
            <div className="mt-6 bg-[rgba(37,161,175,0.1)] rounded-md border-2 border-dashed border-green-500 py-3.5 px-5 text-center text-green-700">
              <Link href="/signup">
                <a className="font-serif text-lg">Click here</a>
              </Link>
              <p className="text-sm">
                to sign up for the complete Examine Membership. Includes a free
                2 week trial!
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginCard;
