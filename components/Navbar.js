import Image from "next/image";
import Link from "next/link";

const Navbar = () => {
  return (
    <nav className="hidden lg:flex shadow-md items-center">
      <div className="px-6 pr-12">
        <Image
          src="/examine-logo.png"
          alt="Examine logo"
          width="140"
          height="31.22"
        />
      </div>
      <div className="grow shadow-inner block py-8 px-6 bg-gray-100">
        <form className="flex items-center gap-3">
          <Image
            src="/magnifying-glass.png"
            alt="magnifying glass"
            width="20"
            height="20"
          />
          <input
            type="search"
            id="nav-search"
            placeholder="What do you want to learn about?"
            className="w-full lg:w-1/2 border-b-2 border-purple-200 focus:outline-none bg-gray-100"
          />
        </form>
      </div>
      <div className="flex items-center gap-x-6 ml-6">
        <Link href="/">
          <a>
            <span>Login</span>
          </a>
        </Link>
        <Link href="/signup">
          <a>
            <span className="bg-premium text-primary rounded-full py-2.5 px-5 font-bold">
              Sign up
            </span>
          </a>
        </Link>
      </div>
      <div className="px-6">
        <Image
          src="/hamburger-desktop.png"
          alt="menu icon"
          width="18"
          height="12"
        />
      </div>
    </nav>
  );
};

export default Navbar;
