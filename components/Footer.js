import Image from "next/image";

const Footer = () => {
  return (
    <footer className="hidden lg:block bg-primary pt-20 pb-12">
      <div className="container">
        <div className="flex">
          <div className="grow">
            <Image
              src="/examine-footer-logo.png"
              alt="Examine logo"
              width="148"
              height="33"
            />
            <div className="mt-10 mr-10 max-w-sm">
              <h3 className="text-3xl font-serif text-white">
                Never miss an update!
              </h3>
              <p className="text-lg text-white">
                We are always updating our research and adding new stuff.
                Sign-up nad always stay on top of our updates!
              </p>
              <form action="" className="mt-8">
                <div className="border-white border-2 py-2 pl-6 pr-3 rounded-full flex items-center justify-between">
                  <input
                    type="text"
                    name=""
                    placeholder="Your Email Address"
                    id="newsletter-sub"
                    className="bg-primary focus:outline-none"
                  />
                  <button>
                    <Image
                      src="/subscribe-btn.png"
                      alt="subscribe"
                      width="46"
                      height="46"
                    />
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div className="mr-20">
            <h4 className="text-2xl font-serif text-white">About Us</h4>
            <div className="flex flex-col text-white">
              <a href="/" className="mt-8">
                Our Story
              </a>
              <a href="/" className="mt-8">
                Our Team
              </a>
              <a href="/" className="mt-8">
                Our Editorial Policy
              </a>
              <a href="/" className="mt-8">
                Our Mistakes
              </a>
            </div>
          </div>
          <div className="mr-12">
            <h4 className="text-2xl font-serif text-white">Resources</h4>
            <div className="flex flex-col text-white">
              <a href="/" className="mt-8">
                Continuing Education
              </a>
              <a href="/" className="mt-8">
                For Journalists
              </a>
              <a href="/" className="mt-8">
                Student Discount
              </a>
              <a href="/" className="mt-8">
                Senior Discount
              </a>
            </div>
          </div>
          <div>
            <h4 className="text-2xl font-serif text-white">Social</h4>
            <div className="flex flex-col text-white">
              <a href="/" className="mt-8">
                Facebook
              </a>
              <a href="/" className="mt-8">
                Twitter
              </a>
              <a href="/" className="mt-8">
                Linkedin
              </a>
              <a href="/" className="mt-8">
                Email Us
              </a>
            </div>
          </div>
        </div>
        <div className="mt-20 flex justify-between border-t-2 border-[#00000022] py-6">
          <div className="text-white">Copyright 2010 - 2021 Examine Inc.</div>
          <div className="text-white">Privacy Policy | Website Terms</div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
