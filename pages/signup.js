import Layout from "../components/Layout";
import SignupCard from "../components/SignupCard";

const SignUp = () => {
  return (
    <Layout>
      <SignupCard />
    </Layout>
  );
};

export default SignUp;
