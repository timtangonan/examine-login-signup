import Layout from "../components/Layout";
import LoginCard from "../components/LoginCard";

export default function Home() {
  return (
    <Layout>
      <LoginCard />
    </Layout>
  );
}
