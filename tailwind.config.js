/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    container: (theme) => ({
      center: true,
      padding: theme("spacing.4"),
    }),
    fontFamily: {
      'body': ["Inter"],
      'serif': ["Lora"]
    },
    colors: {
      'white': "#FFFFFF",
      'gray': {
        100: "#F8F9FA",
        400: "#CED4DA",
        600: "#6C757D"
      },
      'purple': {
        100: "#EFEBF2",
        200: "#C9BDD2",
        400: "#826595",
        600: "#562E69",
      },
      'green': {
        500: "#25A1AF",
        700: "#1B818C"
      }
    },
    extend: {
      colors: {
        primary: "#5E3B76",
        premium: "#FFD747",
      },
    },
  },
  plugins: [],
};
